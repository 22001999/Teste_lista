//
//  ViewController.swift
//  Teste_lista
//
//  Created by COTEMIG on 28/04/22.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 10
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // antes era let cell = Uitableviewcell()
        // depois de trocar eu vou na "Celula" e troco o nome no identifier
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaChave", for: <#T##IndexPath#>)
        
        // somente as linhas pares
        if indexPath.row % 2 == 0{
        cell.backgroundColor = .blue
        }
        
        return cell
    }
    

    @IBOutlet weak var tabela: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tabela.dataSource = self
    }


}

